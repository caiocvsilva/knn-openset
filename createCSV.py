import csv
import glob
import errno

myData = []
myData.append(['number of classes','knn','Acurracy','Accuracy Unknowns','f measure macro','f measure micro'])


path = 'CaioTesteCV/*'
files = glob.glob(path)
for name in files:
    try:
        with open(name) as f:
            content = f.readlines()
            linha1 = content[0]
            linha2 = content[1]
            lixo,classe,lixo,knn = linha1.split(',')
            # print (int(classe))
            # print(int(knn))
            # print(linha2)
            lixo,dados = linha2.split('(')
            accK,accU, fM, fm = dados.split(',')
            fm, lixo = fm.split(')')
            # print(float(accK))
            # print(float(accU))
            # print(float(fM))
            # print(float(fm))
            myData.append([int(classe),int(knn),float(accK),float(accU),float(fM),float(fm)])
            pass # do what you want
    except IOError as exc:
        if exc.errno != errno.EISDIR:
            raise
    f.close()

# print(myData)

# myData = [["first_name", "second_name", "Grade"],
#           ['Alex', 'Brian', 'A'],
#           ['Tom', 'Smith', 'B']]
#
myFile = open('CV.csv', 'w')
with myFile:
    writer = csv.writer(myFile)
    writer.writerows(myData)

print("Writing complete")
